﻿using Microsoft.AspNetCore.Mvc;
using OdeToFood.Model;
using OdeToFood.Models;

namespace OdeToFood.Controllers
{
    public class HomeController:Controller
    {
        private readonly IRepository _repository;

        public HomeController(IRepository repository)
        {
            _repository = repository;
        }

        public IActionResult Index()
        {
            return View();
        }


        public IActionResult Detail(int id)
        {
            Resturant resturant   = _repository.GetResturant(id);
            return View(resturant);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ResturantMv resturantModel)
        {
            var resturant = new Resturant
            {
                      Name = resturantModel.Name,
                      CusineType = resturantModel.CusineType
            };

            return RedirectToAction("Detail", new {id = resturant.Id});
        }

    }
}