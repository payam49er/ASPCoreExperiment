﻿namespace OdeToFood.Models
{
    public enum CusineType
    {
        Italian,
        French,
        Japanese,
        American
    }
    public class Resturant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CusineType CusineType { get; set; }
    }
}