﻿using System.Collections.Generic;
using System.Linq;
using OdeToFood.Model;
using OdeToFood.Models;

namespace OdeToFood
{
    public interface IRepository
    {
        Resturant GetResturant(int id);
        List<Resturant> GetAll();
        void Add(ResturantMv resturant);
    }
    public class Repository:IRepository
    {
        private readonly List<Resturant> resturants = new List<Resturant>
        {
                  new Resturant
                  {
                      Id = 1,
                      Name = "Central Kitchen",
                      CusineType = CusineType.American
                  },
            new Resturant
            {
                Id = 2,
                Name = "Life Alive",
                CusineType = CusineType.American
            },
            new Resturant
            {
                Id=3,
                Name = "Craig on Main",
                CusineType = CusineType.French
            }
        };

        public Resturant GetResturant(int id)
        {
            return resturants.Where(x => x.Id == id).FirstOrDefault();
        }

        public List<Resturant> GetAll()
        {
            return resturants;
        }

        public void Add(ResturantMv resturant)
        {
            Resturant freshResturant = new Resturant
            {
                Name = resturant.Name,
                CusineType = resturant.CusineType,
                Id = resturants.Max(x=>x.Id)+1
            };

            resturants.Add(freshResturant);
        }
    }
}